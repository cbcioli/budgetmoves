// Christopher Cioli
// P&P5 - 1805
// CategoryContract.java

package com.example.budgetmoveslibrary;

public class CategoryContract {

    // Constants
    public static final String CATEGORY_ARGUMENT = "CATEGORY_ARGUMENT";
    public static final String STORING_DATA_PATH = "/category_list";
    public static final String CATEGORY_KEY = "CATEGORY_KEY";
}
