// Christopher Cioli
// P&P5 - 1805
// AmountValueFormatter.java

package com.example.christophercioli.budgetmoves.chart_formatters;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

public class AmountValueFormatter implements IValueFormatter {

    // Member Variables
    private DecimalFormat mFormat;

    public AmountValueFormatter() {
        mFormat = new DecimalFormat("###,###.00");
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex,
                                    ViewPortHandler viewPortHandler) {
        return "$" + mFormat.format(value);
    }
}
