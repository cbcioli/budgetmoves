package com.example.christophercioli.budgetmoves.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.budgetmoveslibrary.AggregateTransaction;
import com.example.christophercioli.budgetmoves.R;
import com.example.christophercioli.budgetmoves.chart_formatters.AmountValueFormatter;
import com.example.christophercioli.budgetmoves.chart_formatters.YAxisValueFormatter;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class ChartsActivity extends AppCompatActivity {

    // Model Objects
    private ArrayList<AggregateTransaction> mAgTx = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charts);

        mAgTx = (ArrayList<AggregateTransaction>) getIntent()
                .getSerializableExtra(MainActivity.AGTX_EXTRA);

        SimpleDateFormat labelFormat = new SimpleDateFormat("MMMM yyyy", Locale.US);
        TextView titleTextView = findViewById(R.id.titleTextView);
        titleTextView.setText(String.format("%s Transactions", labelFormat.format(new Date())));

        setUpChart();
    }

    private void setUpChart() {
        BarChart txBarChart = findViewById(R.id.txBarChart);

        List<IBarDataSet> barDataSets = new ArrayList<>();

        for (int i = 0; i < mAgTx.size(); i++) {
            ArrayList<BarEntry> entries = new ArrayList<>();
            entries.add(new BarEntry((float)i, mAgTx.get(i).getTotalAmount()));
            BarDataSet set = new BarDataSet(entries, mAgTx.get(i).getCategoryName());
            if (!(i >= ColorTemplate.COLORFUL_COLORS.length)) {
                set.setColor(ColorTemplate.COLORFUL_COLORS[i]);
            }

            barDataSets.add(set);
        }


        BarData data = new BarData(barDataSets);
        data.setValueTextSize(10f);
        data.setValueFormatter(new AmountValueFormatter());
        data.setBarWidth(0.9f);
        txBarChart.setData(data);
        txBarChart.setNoDataText("No transactions recorded!");

        txBarChart.getLegend().setTextSize(15f);
        txBarChart.getLegend().setWordWrapEnabled(true);
        txBarChart.getLegend().setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);

        txBarChart.getXAxis().setDrawGridLines(false);
        txBarChart.getXAxis().setDrawLabels(false);
        txBarChart.getAxisLeft().setValueFormatter(new YAxisValueFormatter());
        txBarChart.getAxisRight().setDrawLabels(false);

        txBarChart.setScaleEnabled(false);
        txBarChart.setHighlightPerDragEnabled(false);
        txBarChart.setHighlightPerTapEnabled(true);
        txBarChart.setFitBars(true);
        Description d = new Description();
        d.setText("");
        txBarChart.setDescription(d);

        txBarChart.invalidate();
    }
}
