package com.example.christophercioli.budgetmoves.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.wear.widget.CircularProgressLayout;
import android.support.wearable.activity.ConfirmationActivity;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.budgetmoveslibrary.Transaction;
import com.example.budgetmoveslibrary.TransactionContract;
import com.example.christophercioli.budgetmoves.R;
import com.example.christophercioli.budgetmoves.adapters.CategoryAdapter;
import com.example.christophercioli.budgetmoves.helpers.CommunicationHelper;

import java.util.ArrayList;

public class CategoryInputActivity extends WearableActivity implements CircularProgressLayout.OnTimerFinishedListener {

    // UI Elements
    private ListView mCategoriesListView;
    private CircularProgressLayout mCircularProgress;
    private TextView mTxSavedTextView;

    // Model Objects
    private Transaction mTransaction;
    private ArrayList<String> mCategories;

    // Constants
    private static final int NEW_CATEGORY_REQUEST_CODE = 52618;
    public static final String NEW_CATEGORY_EXTRA = "NEW_CATEGORY_EXTRA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_input);

        setUpListView();

        mTransaction = (Transaction) getIntent()
                .getSerializableExtra(TransactionContract.TRANSACTION_ARGUMENT);

        mCircularProgress = findViewById(R.id.circular_progress);
        mCircularProgress.setOnTimerFinishedListener(this);
        mCircularProgress.setVisibility(View.GONE);
        mTxSavedTextView = findViewById(R.id.txSavedTextView);
        mTxSavedTextView.setVisibility(View.GONE);

        // Enables Always-on
        setAmbientEnabled();
    }

    private void setUpListView() {
        mCategoriesListView = findViewById(R.id.listViewCategories);
        mCategoriesListView.setVisibility(View.VISIBLE);

        mCategories = new ArrayList<>();
        mCategories.add("Car Maintenance");
        mCategories.add("Entertainment");
        mCategories.add("Gas");
        mCategories.add("Groceries");
        mCategoriesListView.setAdapter(new CategoryAdapter(mCategories, this));

        mCategoriesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    // Add New Category
                    Intent newCategoryIntent = new Intent(CategoryInputActivity.this,
                            CreateNewCategoryActivity.class);
                    startActivityForResult(newCategoryIntent, NEW_CATEGORY_REQUEST_CODE);
                } else {
                    finishTransaction(mCategoriesListView.getAdapter()
                            .getItem(i).toString());
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_CATEGORY_REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                String newCategory = data.getStringExtra(NEW_CATEGORY_EXTRA);
                mCategories.add(0, newCategory);
                mCategoriesListView.setAdapter(new CategoryAdapter(mCategories, this));
            }
        }
    }

    private void finishTransaction(String _category) {
        mTransaction.setCategory(_category);

        // Show Confirmation
        mCategoriesListView.setVisibility(View.GONE);
        mCircularProgress.setVisibility(View.VISIBLE);
        mTxSavedTextView.setVisibility(View.VISIBLE);
        mCircularProgress.setTotalTime(2000);
        mCircularProgress.startTimer();

        // Store Data and Notify Mobile that Transaction is Ready
        CommunicationHelper commHelper = new CommunicationHelper(this);
        commHelper.createPutDataRequest(mTransaction);
    }

    @Override
    public void onTimerFinished(CircularProgressLayout layout) {
        Intent goBackIntent = new Intent(this, MainActivity.class);
        goBackIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(goBackIntent);
        finish();
    }
}
