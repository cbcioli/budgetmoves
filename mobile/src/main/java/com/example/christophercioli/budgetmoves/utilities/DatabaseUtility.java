// Christopher Cioli
// P&P5 - 1805
// DatabaseUtility.java

package com.example.christophercioli.budgetmoves.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.budgetmoveslibrary.AggregateTransaction;
import com.example.budgetmoveslibrary.Category;
import com.example.budgetmoveslibrary.Transaction;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DatabaseUtility extends SQLiteOpenHelper {

    // Constants
    private static final String DATABASE_FILE = "budgetmoves.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "transactions";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_AMOUNT = "amount";
    private static final String COLUMN_CATEGORY = "category";
    private static final String COLUMN_DATE = "date";
    private static final String COLUMN_TRANSACTION_ID = "transaction_id";
    private static final String COLUMN_SUM_AMOUNT = "sum_amount";

    private static final String CREATE_TRANSACTION_TABLE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + "( " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_AMOUNT + " REAL, " + COLUMN_CATEGORY + " TEXT, "
            + COLUMN_DATE + " TEXT, " + COLUMN_TRANSACTION_ID + " INTEGER)";

    private static final String CAT_TABLE_NAME = "categories";
    private static final String CAT_COLUMN_NAME = "name";
    private static final String CAT_COLUMN_LIMIT = "cat_limit";

    private static final String CREATE_CATEGORY_TABLE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + CAT_TABLE_NAME
            + "( " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CAT_COLUMN_NAME + " TEXT, " + CAT_COLUMN_LIMIT + " INTEGER)";

    // Member Variables
    private static DatabaseUtility mInstance = null;
    private SQLiteDatabase mDB;

    private DatabaseUtility(Context _context) {
        super(_context, DATABASE_FILE, null, DATABASE_VERSION);
        mDB = getWritableDatabase();

        if (getAllCategories().size() == 0) {
            insertCategory(new Category("Car Maintenance", 500));
            insertCategory(new Category("Entertainment", 500));
            insertCategory(new Category("Gas", 500));
            insertCategory(new Category("Groceries", 500));
        }
    }

    // Singleton Pattern Getter
    public static DatabaseUtility getInstance(Context _context) {
        if (mInstance == null) {
            mInstance = new DatabaseUtility(_context);
        }

        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TRANSACTION_TABLE_STATEMENT);
        sqLiteDatabase.execSQL(CREATE_CATEGORY_TABLE_STATEMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Fired when changing database versions
    }

    // Transaction Table Methods
    public long insertTransaction(Transaction t) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_AMOUNT, t.getAmount());
        cv.put(COLUMN_CATEGORY, t.getCategory());

        SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd", Locale.US);
        cv.put(COLUMN_DATE, sdf.format(t.getDate()));

        cv.put(COLUMN_TRANSACTION_ID, t.getIdentifier());

        return mDB.insert(TABLE_NAME, null, cv);
    }

    public ArrayList<Transaction> getTransactionsFor(String _twoDigitMonth, String _twoDigitYear) {
        ArrayList<Transaction> transactions = new ArrayList<>();
        String yymm = _twoDigitYear + _twoDigitMonth;
        String selection = COLUMN_DATE + " LIKE ?";
        String[] selectionArgs = {yymm + "%"};

        // return Cursor query here
        Cursor queryCursor = mDB.query(TABLE_NAME, null, selection, selectionArgs,
                null, null, null);

        while (queryCursor.moveToNext()) {
            Transaction t = null;

            Float amount = queryCursor.getFloat(queryCursor.getColumnIndex(COLUMN_AMOUNT));
            String category = queryCursor.getString(queryCursor.getColumnIndex(COLUMN_CATEGORY));
            String date = queryCursor.getString(queryCursor.getColumnIndex(COLUMN_DATE));
            long txId = queryCursor.getLong(queryCursor.getColumnIndex(COLUMN_TRANSACTION_ID));

            int year = Integer.valueOf(date.substring(0, 2));
            int month = Integer.valueOf(date.substring(2, 4));
            int day = Integer.valueOf(date.substring(4, 6));

            Calendar c = Calendar.getInstance();
            c.set(year, month - 1, day);
            Date txDate = c.getTime();

            t = new Transaction(amount, category, txDate, txId);
            transactions.add(t);
        }

        queryCursor.close();

        return transactions;
    }

    public ArrayList<AggregateTransaction> getAggregateTransactionsFor(String _twoDigitMonth,
                                                                       String _twoDigitYear) {
        ArrayList<AggregateTransaction> agTx = new ArrayList<>();
        String yymm = _twoDigitYear + _twoDigitMonth;
        String[] columns = { COLUMN_CATEGORY, "SUM(" + COLUMN_AMOUNT + ") AS " + COLUMN_SUM_AMOUNT };
        String selection = COLUMN_DATE + " LIKE ?";
        String[] selectionArgs = {yymm + "%"};
        String orderBy = COLUMN_SUM_AMOUNT + " ASC";

        Cursor queryCursor = mDB.query(TABLE_NAME, columns, selection, selectionArgs,
                COLUMN_CATEGORY, null, orderBy);

        while(queryCursor.moveToNext()) {
            AggregateTransaction at = null;

            String categoryName = queryCursor.getString(queryCursor.getColumnIndex(COLUMN_CATEGORY));
            Float totalAmount = queryCursor.getFloat(queryCursor.getColumnIndex(COLUMN_SUM_AMOUNT));

            at = new AggregateTransaction(categoryName, totalAmount);

            agTx.add(at);
            Log.i("AGTX", at.getCategoryName() + " " + at.getTotalAmount());
        }

        return agTx;
    }

    // Category Table Methods
    public long insertCategory(Category _c) {
        ContentValues cv = new ContentValues();
        cv.put(CAT_COLUMN_NAME, _c.getName());
        cv.put(CAT_COLUMN_LIMIT, _c.getLimit());

        return mDB.insert(CAT_TABLE_NAME, null, cv);
    }

    public ArrayList<Category> getAllCategories() {
        ArrayList<Category> categories = new ArrayList<>();

        Cursor queryCursor = mDB.query(CAT_TABLE_NAME, null, null,
                null, null, null, null);

        while(queryCursor.moveToNext()) {
            Category c = null;

            long id = queryCursor.getLong(queryCursor.getColumnIndex(COLUMN_ID));
            String name = queryCursor.getString(queryCursor.getColumnIndex(CAT_COLUMN_NAME));
            int limit = queryCursor.getInt(queryCursor.getColumnIndex(CAT_COLUMN_LIMIT));

            c = new Category(id, name, limit);
            categories.add(c);
        }

        return categories;
    }

    public void updateCategoryLimit(Category _c) {
        ArrayList<Category> allCategories = getAllCategories();
        Category matchedCategory = null;

        for (Category cat : allCategories) {
            if (cat.getId() == _c.getId()) {
                matchedCategory = cat;
                break;
            }
        }

        if (matchedCategory == null) {
            throw new IllegalArgumentException("This category does not exist!");
        }

        ContentValues cv = new ContentValues();
        cv.put(CAT_COLUMN_LIMIT, _c.getLimit());

        mDB.update(CAT_TABLE_NAME, cv, COLUMN_ID + " = ?",
                new String[]{ String.valueOf(_c.getId()) });
    }
}
