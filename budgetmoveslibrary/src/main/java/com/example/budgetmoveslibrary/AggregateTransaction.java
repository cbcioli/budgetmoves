// Christopher Cioli
// P&P5 - 1805
// AggregateTransaction.java

package com.example.budgetmoveslibrary;

import java.io.Serializable;

public class AggregateTransaction implements Serializable {

    // Member Variables
    private String mCategoryName;
    private Float mTotalAmount;

    public AggregateTransaction(String _name, Float _amount) {
        mCategoryName = _name;
        mTotalAmount = _amount;
    }

    // Getters
    public String getCategoryName() {
        return mCategoryName;
    }

    public Float getTotalAmount() {
        return mTotalAmount;
    }
}
