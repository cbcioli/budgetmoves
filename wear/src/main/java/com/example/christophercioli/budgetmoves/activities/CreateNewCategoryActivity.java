package com.example.christophercioli.budgetmoves.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.christophercioli.budgetmoves.R;

public class CreateNewCategoryActivity extends WearableActivity {

    // UI Elements
    private EditText mNewCategoryTextView;
    private ImageButton mSaveCategoryImageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_category);

        setUpUiElements();

        // Enables Always-on
        setAmbientEnabled();
    }

    private void setUpUiElements() {
        mNewCategoryTextView = findViewById(R.id.editTextNewCategory);
        mSaveCategoryImageButton = findViewById(R.id.buttonSaveCategory);

        mSaveCategoryImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newCategory = mNewCategoryTextView.getText().toString();
                if (newCategory.trim().isEmpty()) {
                    mNewCategoryTextView.setError(getString(R.string.noCategoryError));
                } else {
                    newCategory = newCategory.substring(0, 1).toUpperCase()
                            + newCategory.substring(1).toLowerCase();

                    Intent backToCategoryInputIntent = new Intent();
                    backToCategoryInputIntent.putExtra(CategoryInputActivity.NEW_CATEGORY_EXTRA,
                            newCategory);
                    setResult(RESULT_OK, backToCategoryInputIntent);
                    finish();
                }
            }
        });
    }
}
