package com.example.christophercioli.budgetmoves.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.budgetmoveslibrary.Transaction;
import com.example.budgetmoveslibrary.TransactionContract;
import com.example.christophercioli.budgetmoves.R;

public class AmountInputActivity extends WearableActivity {

    // UI Elements
    private TextView mEditTextAmount;
    private ImageButton mButtonSaveAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amount_input);

        setUpUIElements();

        // Enables Always-on
        setAmbientEnabled();
    }

    private void setUpUIElements() {
        mEditTextAmount = findViewById(R.id.editTextAmount);

        mButtonSaveAmount = findViewById(R.id.buttonSaveAmount);
        mButtonSaveAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Float f = Float.valueOf(mEditTextAmount.getText().toString());
                Transaction transaction = new Transaction(f);
                Intent categoryIntent = new Intent(AmountInputActivity.this,
                        CategoryInputActivity.class);
                categoryIntent.putExtra(TransactionContract.TRANSACTION_ARGUMENT, transaction);
                startActivity(categoryIntent);
            }
        });
    }
}
