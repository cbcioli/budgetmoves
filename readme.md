# Budget Moves
### by Christopher Cioli
---
Budget Moves is an application designed to help users track their budgets.
---
Devices Tested On (Alpha Build):  
1. Android Wear Round API 26 - 320x320 hdpi - Android 8.0  
2. Nexus 5x Api 25 - 1080x1920: 420dpi - Android 7.1.1  
Devices Tested On (Gold Build):  
1. Huawei Watch 2 (Round) API 27 - Android 8.0.0
2. Essential PH-1 API 27 - Android 8.1.0
---
Repo Link:  
[Budget Moves BitBucket Repo](https://bitbucket.org/cbcioli/budgetmoves/src/master/)
--
Alpha Build Details:  
The Alpha build focuses on the "Create and Save a Transaction" feature for the user.  
The budgetmoveslibrary contains resources and classes that are shared between both mobile and wear projects.  
This feature is implemented in the wear app. The user is able to run the wearable app and create a transaction, set an amount, and set a category for the transaction or create a new category. Testing should be fairly straight forward - The app can be run directly in the wear simulator to see the functionality.

Gold Build Details:
The Gold Build contains features that allow for storing of saved transactions, a listener service that allows transactions from the wearable to be saved on the mobile even when the mobile app is not running, and a new chart that shows the user the breakdown of their transactions for the month by category. A user can create a new category from the wearable device and store a transaction using that category. This will store in the mobile database. However, there is currently no persistence for new Categories.
