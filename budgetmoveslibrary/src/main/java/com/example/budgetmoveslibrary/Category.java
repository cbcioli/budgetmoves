// Christopher Cioli
// P&P5 - 1805
// Category.java

package com.example.budgetmoveslibrary;

import org.apache.commons.lang3.SerializationUtils;

import java.io.Serializable;
import java.util.ArrayList;

public class Category implements Serializable{

    // Member Variables
    private long mId = 0x111100;
    private String mName;
    private int mLimit;

    public Category(String _name, int _limit) {
        mName = _name;
        mLimit = _limit;
    }

    public Category(long _id, String _name, int _limit) {
        mId = _id;
        mName = _name;
        mLimit = _limit;
    }

    // Getters
    public long getId() {
        return mId;
    }
    public String getName() {
        return mName;
    }

    public int getLimit() {
        return mLimit;
    }

    // Setters
    public void setLimit(int _newLimit) {
        mLimit = _newLimit;
    }

    // Apache Lang Library Serializer for Transferring Categories between Wearable and Mobile
    public byte[] convertIntoBytes() {
        return SerializationUtils.serialize(this);
    }

    public static Category convertFromBytes(byte[] _bytes) {
        return SerializationUtils.deserialize(_bytes);
    }

    public static ArrayList<byte[]> covertAllCategoriesIntoBytes(ArrayList<Category> _categories) {
        ArrayList<byte[]> byteArrays = new ArrayList<>();
        for(Category c : _categories) {
            byte[] bytes = c.convertIntoBytes();
            byteArrays.add(bytes);
        }

        return byteArrays;
    }

    public static ArrayList<Category> convertAllCategoriesFromBytes(ArrayList<byte[]> byteArrays) {
        ArrayList<Category> categories = new ArrayList<>();

        for (byte[] b : byteArrays) {
            Category c = convertFromBytes(b);
            categories.add(c);
        }

        return categories;
    }
}
