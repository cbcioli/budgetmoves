// Christopher Cioli
// P&P2 - 1805
// CommunicationHelper

package com.example.christophercioli.budgetmoves.helpers;

import android.content.Context;
import com.example.budgetmoveslibrary.Transaction;
import com.example.budgetmoveslibrary.TransactionContract;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

public class CommunicationHelper {

    // Member Variables
    private Context mContext;
    private DataClient mDataClient;

    public CommunicationHelper(Context _context) {
        mContext = _context;
        mDataClient = Wearable.getDataClient(mContext);
    }

    public void createPutDataRequest(Transaction _transaction) {
        PutDataMapRequest mapRequest = PutDataMapRequest
                .create(TransactionContract.STORING_DATA_PATH);
        mapRequest.getDataMap().putByteArray(TransactionContract.TRANSACTION_KEY,
                _transaction.convertIntoBytes());
        PutDataRequest dataRequest = mapRequest.asPutDataRequest();
        dataRequest.setUrgent();
        Task<DataItem> dataItemTask = mDataClient.putDataItem(dataRequest);
    }

}
