// Christopher Cioli
// P&P5 - 1805
// Transaction.java

package com.example.budgetmoveslibrary;

import android.support.annotation.NonNull;

import org.apache.commons.lang3.SerializationUtils;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

public class Transaction implements Serializable, Comparable<Transaction> {

    // Member Variables
    private Float mAmount;
    private String mCategory;
    private Date mDate;
    private long mIdentifier;

    // Constructors
    public Transaction(Float _amount) {
        mAmount = _amount;
        mCategory = null;
        mDate = new Date();
        mIdentifier = System.currentTimeMillis();
    }

    public Transaction(Float _amount, String _category) {
        mAmount = _amount;
        mCategory = _category;
        mDate = new Date();
        mIdentifier = System.currentTimeMillis();
    }

    public Transaction(Float _amount, String _category, Date _date, long _txId) {
        mAmount = _amount;
        mCategory = _category;
        mDate = _date;
        mIdentifier = _txId;
    }

    // Getters
    public Float getAmount() {
        return mAmount;
    }

    public String getCategory() {
        return mCategory;
    }

    public Date getDate() {
        return mDate;
    }

    public long getIdentifier() { return mIdentifier; }

    // Setters
    public void setAmount(Float _newAmount) {
        mAmount = _newAmount;
    }

    public void setCategory(String _newCategory) {
        mCategory = _newCategory;
    }

    public void setDate(Date _newDate) {
        mDate = _newDate;
    }

    // Apache Lang Library Serializer for Transferring Transactions between Wearable and Mobile
    public byte[] convertIntoBytes() {
        return SerializationUtils.serialize(this);
    }

    public static Transaction convertFromBytes(byte[] _bytes) {
        return SerializationUtils.deserialize(_bytes);
    }

    // Comparable Methods - Reverse Sort
    @Override
    public int compareTo(@NonNull Transaction transaction) {
        return transaction.getDate().compareTo(getDate());
    }
}