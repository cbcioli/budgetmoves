// Christopher Cioli
// P&P5 - 1805
// BudgetWearableListenerService.java

package com.example.christophercioli.budgetmoves.services;

import com.example.budgetmoveslibrary.Transaction;
import com.example.budgetmoveslibrary.TransactionContract;
import com.example.christophercioli.budgetmoves.utilities.DatabaseUtility;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;

public class BudgetWearableListenerService extends WearableListenerService {

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        DatabaseUtility mDBUtility = DatabaseUtility.getInstance(this);
        for (DataEvent event : dataEventBuffer) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                DataItem item = event.getDataItem();
                if (item.getUri().getPath().compareTo(TransactionContract.STORING_DATA_PATH) == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    byte[] inboundBytes = dataMap.getByteArray(TransactionContract.TRANSACTION_KEY);
                    Transaction inboundTransaction = Transaction.convertFromBytes(inboundBytes);
                    mDBUtility.insertTransaction(inboundTransaction);
                }
            }
        }
    }
}
