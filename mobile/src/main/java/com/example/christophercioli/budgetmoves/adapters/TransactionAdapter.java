package com.example.christophercioli.budgetmoves.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.budgetmoveslibrary.Transaction;
import com.example.christophercioli.budgetmoves.R;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class TransactionAdapter extends BaseAdapter {

    // Model Objects
    ArrayList<Transaction> mTransactions;
    Context mContext;

    public TransactionAdapter(ArrayList<Transaction> _transactions, Context _context) {
        mTransactions = _transactions;
        mContext = _context;
    }


    @Override
    public int getCount() {
        if (mTransactions != null) {
            return mTransactions.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        if (mTransactions != null) {
            return mTransactions.get(i);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return 0x0110 + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder vh;

        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.transaction_cell, viewGroup,
                    false);
            vh = new ViewHolder(view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) view.getTag();
        }

        Transaction t = (Transaction) getItem(i);

        if (t != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);
            vh.mDateTextView.setText(sdf.format(t.getDate()));
            vh.mCategoryTextView.setText(t.getCategory());

            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
            String formattedAmount = format.format(t.getAmount());

            vh.mAmountTextView.setText(formattedAmount);
        }

        return view;
    }

    static class ViewHolder {

        // UI Elements
        private TextView mDateTextView;
        private TextView mCategoryTextView;
        private TextView mAmountTextView;

        public ViewHolder(View _layout) {
            mDateTextView = _layout.findViewById(R.id.dateTextView);
            mCategoryTextView = _layout.findViewById(R.id.categoryTextView);
            mAmountTextView = _layout.findViewById(R.id.amountTextView);
        }
    }
}
