package com.example.christophercioli.budgetmoves.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.christophercioli.budgetmoves.R;

import java.util.ArrayList;

public class CategoryAdapter extends BaseAdapter {
    // Model Objects
    private ArrayList<String> mCategories;
    private Context mContext;

    public CategoryAdapter(ArrayList<String> _categories, Context _context) {
        mCategories = _categories;
        mContext = _context;
    }

    @Override
    public int getCount() {
        if (mCategories != null) {
            return mCategories.size() + 1;
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        if (mCategories != null) {
            return mCategories.get(i - 1);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return 0x1110 + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder vh;

        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.category_cell,
                    viewGroup, false);
            vh = new ViewHolder(view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) view.getTag();
        }

        if (i == 0) {
            vh.textView.setText(R.string.addNewCategory);
            vh.imageView.setVisibility(View.VISIBLE);
            vh.imageView.setImageDrawable(mContext
                    .getDrawable(R.drawable.ic_add_circle_white_24dp));
        } else {
            vh.imageView.setVisibility(View.GONE);
            vh.textView.setText(mCategories.get(i - 1));
        }

        return view;
    }

    static class ViewHolder {

        // UI Elements
        private TextView textView;
        private ImageView imageView;

        public ViewHolder(View _layout) {
            textView = _layout.findViewById(R.id.categoryTextView);
            imageView = _layout.findViewById(R.id.categoryImageView);
        }
    }
}
