// Christopher Cioli
// P&P5 - 1805
// TransactionContract.java

package com.example.budgetmoveslibrary;

public class TransactionContract {

    // Constants
    public static final String TRANSACTION_ARGUMENT = "TRANSACTION_ARGUMENT";
    public static final String STORING_DATA_PATH = "/new_user_transaction";
    public static final String TRANSACTION_KEY = "TRANSACTION_KEY";
}
