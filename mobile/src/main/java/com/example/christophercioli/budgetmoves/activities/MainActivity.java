// Christopher Cioli
// P&P5 - 1805
// MainActivity.java

package com.example.christophercioli.budgetmoves.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.ListView;
import android.widget.TextView;

import com.example.budgetmoveslibrary.AggregateTransaction;
import com.example.budgetmoveslibrary.Transaction;
import com.example.budgetmoveslibrary.TransactionContract;
import com.example.christophercioli.budgetmoves.R;
import com.example.christophercioli.budgetmoves.adapters.TransactionAdapter;
import com.example.christophercioli.budgetmoves.utilities.DatabaseUtility;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Wearable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements DataClient.OnDataChangedListener {

    // Model Objects
    private ArrayList<Transaction> mTransactions = new ArrayList<>();
    private DatabaseUtility mDBUtility;

    // UI Elements
    private TextView mMonthTextView;
    private ListView mTransactionListView;
    private ViewStub mEmptyStateStub;

    // Constants
    public static final String AGTX_EXTRA = "AGTX_EXTRA";

    // Activity Lifecycle Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDBUtility = DatabaseUtility.getInstance(this);

        SimpleDateFormat txFormat = new SimpleDateFormat("yyMM", Locale.US);
        String queryDate = txFormat.format(new Date());
        mTransactions = mDBUtility.getTransactionsFor(queryDate.substring(2, 4),
                queryDate.substring(0, 2));

        // Sort bt date - Newest -> Oldest
        Collections.sort(mTransactions);

        setUpActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Wearable.getDataClient(this).addListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Wearable.getDataClient(this).addListener(this);
    }

    // Menu Methods


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        SimpleDateFormat txFormat = new SimpleDateFormat("yyMM", Locale.US);
        String queryDate = txFormat.format(new Date());
        ArrayList<AggregateTransaction> agtx = mDBUtility.getAggregateTransactionsFor(
                queryDate.substring(2, 4), queryDate.substring(0, 2));

        Intent chartsIntent = new Intent(this, ChartsActivity.class);
        chartsIntent.putExtra(AGTX_EXTRA, agtx);
        startActivity(chartsIntent);

        return true;
    }

    // UI Setup
    private void setUpActivity() {
        mMonthTextView = findViewById(R.id.monthTextView);
        mTransactionListView = findViewById(R.id.transactionListView);
        mEmptyStateStub = findViewById(R.id.noTransactionsViewStub);

        SimpleDateFormat labelFormat = new SimpleDateFormat("MMMM yyyy", Locale.US);
        mMonthTextView.setText(labelFormat.format(new Date()));

        showListView(!(mTransactions.size() == 0));
        mTransactionListView.setAdapter(new TransactionAdapter(mTransactions, this));
    }

    // DataClient Methods
    // Note: This method does not update the database with the received transaction.
    // It instead adds the transaction to the local collections and lets the
    // BudgetWearableListenerService handle interaction with the database.
    @Override
    public void onDataChanged(@NonNull DataEventBuffer dataEventBuffer) {
        for (DataEvent event : dataEventBuffer) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                DataItem item = event.getDataItem();
                if (item.getUri().getPath().compareTo(TransactionContract.STORING_DATA_PATH) == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    byte[] inboundBytes = dataMap.getByteArray(TransactionContract.TRANSACTION_KEY);
                    Transaction inboundTransaction = Transaction.convertFromBytes(inboundBytes);
                    updateLocalTransactionList(inboundTransaction);
                }
            }
        }
    }

    // Add incoming transaction to list
    private void updateLocalTransactionList(Transaction _transaction) {
        mTransactions.add(_transaction);
        Collections.sort(mTransactions);
        mTransactionListView.setAdapter(new TransactionAdapter(mTransactions, this));
        showListView(true);
    }

    // Show or Hide ListView
    private void showListView(boolean _show) {

        if (_show) {
            mMonthTextView.setVisibility(View.VISIBLE);
            mTransactionListView.setVisibility(View.VISIBLE);
            mEmptyStateStub.setVisibility(View.GONE);
        } else {
            mMonthTextView.setVisibility(View.GONE);
            mTransactionListView.setVisibility(View.GONE);
            mEmptyStateStub.setVisibility(View.VISIBLE);
        }
    }
}
