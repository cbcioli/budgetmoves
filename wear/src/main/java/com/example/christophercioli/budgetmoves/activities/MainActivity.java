package com.example.christophercioli.budgetmoves.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.ImageButton;

import com.example.christophercioli.budgetmoves.R;

public class MainActivity extends WearableActivity {

    // UI Elements
    private ImageButton mButtonAddTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpUIElements();

        // Enables Always-on
        setAmbientEnabled();
    }

    private void setUpUIElements() {
        mButtonAddTransaction = findViewById(R.id.buttonAddTransaction);
        mButtonAddTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent amountIntent = new Intent(MainActivity.this,
                        AmountInputActivity.class);
                startActivity(amountIntent);
            }
        });
    }
}
