// Christopher Cioli
// P&P5 - 1805
// YAxisValueFormatter.java

package com.example.christophercioli.budgetmoves.chart_formatters;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import java.text.DecimalFormat;

public class YAxisValueFormatter implements IAxisValueFormatter {
    private DecimalFormat mFormat;

    public YAxisValueFormatter() {
        mFormat = new DecimalFormat("###,###.00");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return "$" + mFormat.format(value);
    }
}
